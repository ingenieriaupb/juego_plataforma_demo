﻿using UnityEngine;
using System.Collections;

public class Personaje : MonoBehaviour {

	Animator anim;
	private GameObject hero;
	public Vector3 speed = new Vector3();
	bool aterrizo = true;
	PlayerStateController.playerStates estadoActual;

	void Start(){
		hero = GameObject.Find("heroe");
		PlayerStateController.onStateChange += prueba;
		anim = hero.GetComponent<Animator> ();
	}

	public void prueba(PlayerStateController.playerStates estado){
		if (estado == estadoActual)
			return;

		estadoActual = estado;
		Debug.Log (estadoActual);
	}

	void OnCollisionEnter2D(Collision2D c){
		/*if (c.collider.CompareTag("coin") ){
			Destroy(c.collider.gameObject);
		}*/
		if (c.collider.CompareTag ("piso")) {
			aterrizo = true;
		}
	}

	void Update(){
		float jumpDirection = 0f;
		Rigidbody2D rb;
			
		switch(estadoActual){
		case PlayerStateController.playerStates.right:
			hero.transform.Translate( speed * Time.deltaTime);
			hero.transform.localScale = new Vector3(1.0f,1,0);
			anim.SetInteger("estadoActual",1);
			break;
		case PlayerStateController.playerStates.left:
			hero.transform.Translate( speed * Time.deltaTime * -1.0f);
			hero.transform.localScale = new Vector3(-1.0f,1,0);
			anim.SetInteger("estadoActual",1);
			break;
		case PlayerStateController.playerStates.jump:
			if(aterrizo){
				if(estadoActual == PlayerStateController.playerStates.left){
					jumpDirection = -1f;
				}else if(estadoActual == PlayerStateController.playerStates.right)
				{
					jumpDirection = 1f;
				}
				else{
					jumpDirection = 0f;
				}
				rb = hero.GetComponent<Rigidbody2D>();
				rb.AddForce(new Vector2(jumpDirection*250f,500f));
				aterrizo = false;
				anim.SetInteger("estadoActual",2);
			}

			break;
		case PlayerStateController.playerStates.idle:
			rb = hero.GetComponent<Rigidbody2D>();
			rb.AddForce(new Vector2(0,-50f));
			anim.SetInteger("estadoActual",0);
			break;
		}
		/*
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			hero.transform.Translate( speed * Time.deltaTime);
		}

		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			hero.transform.Translate( speed * Time.deltaTime * -1.0f);
		}*/
	}

}
