﻿using UnityEngine;
using System.Collections;

public class PlayerStateController : MonoBehaviour {

	public enum playerStates{ idle = 0,left,right,jump,landing,falling,kill,resurrect}
	public delegate void playerStateHandler(PlayerStateController.playerStates newState);
	public static event playerStateHandler onStateChange;

	void LateUpdate(){
		float horizontal = Input.GetAxis ("Horizontal");
		float jump = Input.GetAxis ("Jump");

		if (horizontal != 0f) {

			if(onStateChange != null && horizontal < 0f )
			{
				onStateChange(PlayerStateController.playerStates.left);
			}
			else if(onStateChange != null && horizontal > 0f)
			{
				onStateChange(PlayerStateController.playerStates.right);
			}
		}
		else{
			if(onStateChange != null)
			{
				onStateChange(PlayerStateController.playerStates.idle);
			}
		}

		if (jump > 0f) {
			if(onStateChange != null)
			{
				onStateChange(PlayerStateController.playerStates.jump);
			}
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
